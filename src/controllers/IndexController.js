const api = require("../services/api");

module.exports = {
  async index(req, res) {
    const { data } = await api.get("/");

    //select only websites
    const usersWebsites = data.map(user => user.website);

    //select name, email and company order by name

    const nameEmailCompany = data
      .sort(function(a, b) {
        return a.name < b.name ? -1 : a.name > b.name ? 1 : 0;
      })
      .map(user => {
        const { name, email, company } = user;
        const info = {
          name,
          email,
          company
        };
        return info;
      });

    //Show all users other than address contain the word `` `suite``
    const usersAddressSuite = data.filter(user => {
      const { address } = user;
      const suite = address.suite.indexOf("Suite");
      return suite !== -1;
    });

    res.json({ usersWebsites, nameEmailCompany, usersAddressSuite });
  }
};
