const axios = require("axios");

const api = axios.create({
  baseURL: "https://jsonplaceholder.typicode.com/users",
  headers: {
    "Content-Type": "application/json"
  }
});

module.exports = api;
