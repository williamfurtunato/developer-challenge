const express = require("express");

const routes = new express.Router();

const IndexController = require("./controllers/IndexController");

routes.get("/", IndexController.index);

module.exports = routes;
